#!/usr/bin/env node

var argv = require('minimist')(process.argv.slice(2));
var Table = require('cli-table');
var Storm = require('./storm');
var log = require('./lib/log');
var table = new Table({
    head: ['url', 'min (ms)', 'max (ms)', 'average (ms)']
});

var storm = new Storm(argv, function(results) {

    results.sort(function(a, b) {
        if (a[0] > b[0]) {
            return 1;
        }
        if (a[0] < b[0]) {
            return -1;
        }
        return 0;
    });

    results.forEach(function(item) {
        table.push(item);
    });

    console.log(table.toString());
});

process.on('SIGINT', function() {
    console.log('Got a SIGINT. Goodbye cruel world');
    process.exit(0);
});
