var events = require('events');
var eventEmitter = new events.EventEmitter();
var utils = require('./lib/utils');
var cluster = require('cluster');
var phantom = require('phantom');
var log = require('./lib/log');
var _ = require('lazy.js');
var fs = require('fs');

function Storm(argv, callback) {
    var self = this;
    var options = {
        c: 4, // Concurrency
        r: 100, // Requests
        _: 'http://google.co.uk',
        f: null // File
    };

    /*
     * Extend with user options
     */

    for (var key in argv) {
        if (argv.hasOwnProperty(key)) {
            options[key] = argv[key];
        }
    }

    this.argv = argv;
    this.options = options;
    this.workerId = 0;
    this.workers = {};
    this.completed = 0;
    this.results = {};
    this.callback = callback || function() {};

    for (var i = 0; i < options.c; i++) {
        this.worker();
    }

    this.createQueue();

    eventEmitter.on('workerConnected', function(worker) {
        if (self.queue.length) {
            self.open(worker, self.queue.shift());
        }
    });

    eventEmitter.on('pageDone', function(data) {
        self.completed++;
        data.worker.completed++;
        (!self.results[data.result.url]) ? self.results[data.result.url] = [data.result.loadtime] : self.results[data.result.url].push(data.result.loadtime);

        if (data.worker.completed === 50) {
            log.info('Cycling worker...');
            data.worker.phantom.exit();
            return self.worker();
        }

        if (self.queue.length) {
            self.open(data.worker, self.queue.shift());
        } else if (self.completed === self.options.r) {
            self.exit();
        }
    });
}

Storm.prototype.worker = function() {
    var self = this;
    log.info('Worker spawned...');

    phantom.create(function(ph) {
        ++self.workerId;

        self.workers[self.workerId] = {
            _id: self.workerId,
            phantom: ph,
            completed: 0
        };

        eventEmitter.emit('workerConnected', self.workers[self.workerId]);
    }, {
        onExit: function (code) {
            if (code !== 0) {
                self.worker();
            }
        }
    });
};

Storm.prototype.createQueue = function() {
    this.queue = [];
    var urls = (this.options.f) ? utils.getUrls(this.options.f) : [this.options._[0]];

    var i = 0;
    while (this.queue.length < this.options.r) {
        this.queue.push(urls[i]);
        i = (i + 1) % urls.length;
    }
};

Storm.prototype.open = function(worker, url) {
    worker.phantom.createPage(function(page) {
        var t = Date.now();
        page.open(url, function(status) {
            log.info('Getting =>', url, '\nStatus =>', status);
            page.evaluate((function() {
                return {
                    time: Date.now()
                };
            }), function(result) {
                result.loadtime = result.time - t;
                result.url = url;

                eventEmitter.emit('pageDone', {
                    worker: worker,
                    result: result
                });
            });
        });
    });
};

Storm.prototype.exit = function(worker, url) {
    var self = this;
    var data = [];

    function add(a, b) {
        return a + b;
    }

    Object.keys(self.results).forEach(function(key) {
        data.push([
            key, // Url
            _(self.results[key]).min(), // Min
            _(self.results[key]).max(), // Max
            (self.results[key].reduce(add, 0) / self.results[key].length) // Average
        ]);
    });

    self.callback(data);

    Object.keys(self.workers).forEach(function(id) {
        self.workers[id].phantom.exit(0);
    });

};

module.exports = Storm;
