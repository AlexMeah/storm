/*global module:false*/
module.exports = function(grunt) {

    require('jit-grunt')(grunt);

    // Project configuration.
    grunt.initConfig({
        // Task configuration.
        jshint: {
            gruntfile: {
                src: 'Gruntfile.js'
            },
            lib_test: {
                src: ['lib/**/*.js', 'test/**/*.js']
            }
        },
        mochaTest: {
            test: {
                options: {
                    reporter: 'spec',
                    require: 'lib/blanket'

                },
                src: ['test/**/*_test.js']
            }
        },
        watch: {
            js: {
                files: ['{lib,test}/**/*.js'],
            }
        }
    });

    // Default task.
    grunt.registerTask('default', ['jshint', 'mochaTest']);
};
