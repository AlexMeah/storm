var chalk = require('chalk');
var utils = require('util');

function format(input) {
    if (typeof input === 'string') return input;
    return utils.inspect(input);
}

function parse(args) {
    var arr = [];

    for (var i = 0; i < args.length; i++) {
        arr.push(format(args[i]));
    }

    return arr.join(' ');
}

var log = function() {
    this.args = parse(arguments);

    console.log(this.args);
    return this;
};

log.warn = function() {
    this.args = parse(arguments);

    console.warn(chalk.yellow(this.args));
    return this;
};

log.error = function() {
    this.args = parse(arguments);

    console.error(chalk.red(this.args));
    return this;
};

log.info = function() {
    this.args = parse(arguments);

    console.info(chalk.cyan(this.args));
    return this;
};

log.danger = function() {
    this.args = parse(arguments);

    console.log(chalk.red.bold.underline(this.args));
    return this;
};

module.exports = log;
