var fs = require('fs');
var log = require('./log');

var utils = {};

utils.readFile = function(path, callback) {

    if (typeof callback === 'function') {
        fs.readFile(path, 'utf-8', callback);
    } else {
        return fs.readFileSync(path, 'utf-8');
    }
};

utils.validator = require('validator');

utils.getUrls = function(file, func) {
    var result = utils.readFile(file);

    result = result.split('\n');

    result = result.filter(function(line) {
        if (utils.validator.isURL(line)) {
            return true;
        } else {
            log.warn('Dropping', line, 'invalid url');
            return false;
        }
    });
    return result;
};

module.exports = utils;
