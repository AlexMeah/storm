require('chai').should();
var log = require('../lib/log');

describe('Custom logger', function() {

    it('should log correct string to the console', function() {
        log.warn('Hello').args.should.equal('Hello');
        log.error('Hello').args.should.equal('Hello');
        log.info('Hello').args.should.equal('Hello');
        log.danger('Hello').args.should.equal('Hello');
        log('Hello').args.should.equal('Hello');
    });

    it('should handle objects', function() {
        var obj = {
            hello: true
        };

        log.warn(obj).args.should.deep.equal('{ hello: true }');
        log.error(obj).args.should.deep.equal('{ hello: true }');
        log.info(obj).args.should.deep.equal('{ hello: true }');
        log.danger(obj).args.should.deep.equal('{ hello: true }');
        log(obj).args.should.deep.equal('{ hello: true }');
    });
});
