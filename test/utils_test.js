require('chai').should();
var utils = require('../lib/utils');

describe('Utils', function() {

    describe('readFile', function() {

        it('should return the file contents if no callback is passed', function() {
            var data = utils.readFile('test/fixtures/urlList.txt');

            data.should.contain('alexmeah');
        });

        it('should be async and call the callback with an err if one occurs', function(done) {
            utils.readFile('test/fixtures/urlList.txt', function(err, data) {
                data.should.contain('alexmeah');
                done();
            });
        });

        it('should be async and call the callback with the file contents', function(done) {
            utils.readFile('test/fixtures/urlList.txt', function(err, data) {
                data.should.contain('alexmeah');
                done();
            });
        });
    });

    describe('getUrls', function() {
        it('should strip out any suspect urls', function() {
            var urls = utils.getUrls('test/fixtures/urlList.txt');
            urls.length.should.equal(4);
            urls.should.not.contain('alex meah');
        });
    });
});
