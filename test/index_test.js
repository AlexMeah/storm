require('chai').should();
var expect = require('chai').expect;
var Storm = require('../storm');

describe('Storm', function() {
    it('should have better tests :)', function(done) {
        this.timeout(9999999);
        var storm = new Storm({
            c: 1, // Concurrency
            r: 1, // Requests
            _: ['http://google.co.uk']
        }, function(results) {
            expect(results.length).to.equal(1);
            expect(results[0][0]).to.have.string('http://google.co.uk');
            done();
        });
    });
});
